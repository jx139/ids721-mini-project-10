use serde::{Deserialize, Serialize};
use warp::http::StatusCode;
use warp::Filter;

#[derive(Deserialize)]
struct Request {
    input: String,
}

#[derive(Serialize)]
struct Response {
    message: String,
}

#[tokio::main]
async fn main() {
    let transformer_route = warp::post()
        .and(warp::path("transform"))
        .and(warp::body::json())
        .and_then(handle_transform);

    warp::serve(transformer_route)
        .run(([127, 0, 0, 1], 3030))
        .await;
}

async fn handle_transform(body: Request) -> Result<impl warp::Reply, warp::Rejection> {
    // Placeholder for transformer logic, just echoing input for now
    let response = Response {
        message: format!("Transformed: {}", body.input),
    };
    Ok(warp::reply::with_status(
        warp::reply::json(&response),
        StatusCode::OK,
    ))
}
