# Rust Serverless Transformer Endpoint
![pipeline status](https://gitlab.oit.duke.edu/jx139/ids721-mini-project-10/badges/main/pipeline.svg)

This project sets up a serverless API endpoint using AWS Lambda and Amazon API Gateway to handle HTTP POST requests that transform input text. The transformation logic is written in Rust and deployed in a containerized environment using Docker.

## Prerequisites

- AWS CLI installed and configured with proper access rights.
- Docker installed on your local machine.
- Rust programming language environment.

## Setup Steps

### 1. Implement the Rust Application

The Rust application uses Warp for creating an HTTP server. Your application logic should reside in `handle_transform` function.

- `src/main.rs`: Main application file with Warp server setup.

### 2. Dockerize the Rust Application

Create a `Dockerfile` in the root of your project to build and run your Rust application.

- `Dockerfile`: Instructions to build the Rust application Docker image.

### 3. Push Docker Image to Amazon ECR

Use AWS CLI to create an ECR repository and push your Docker image.

```sh
aws ecr create-repository --repository-name rust_transformer
aws ecr get-login-password --region <region> | docker login --username AWS --password-stdin <aws_account_id>.dkr.ecr.<region>.amazonaws.com
docker tag rust_transformer:latest <aws_account_id>.dkr.ecr.<region>.amazonaws.com/rust_transformer:latest
docker push <aws_account_id>.dkr.ecr.<region>.amazonaws.com/rust_transformer:latest
```

![ecr](./pic/ecr.png)
![ecr_login](./pic/ecr_login.png)
![docker_push](./pic/docker_push.png)

### 4. Create Lambda Function with the Container Image

In the AWS Console, create a new Lambda function using the container image pushed to ECR.

![lambda](./pic/lambda.png)
![push_output](./pic/push_output.png)

### 5. Set up API Gateway

Create an API Gateway HTTP API to route requests to your Lambda function.

- Define a `POST` method on `/transform` resource.
- Deploy the API to a stage (e.g., `dev`).

![api](./pic/api.png)

### 6. Test Your Endpoint

Use `curl` to test the deployed Lambda function:

```sh
curl -X POST https://<api_id>.execute-api.<region>.amazonaws.com/<stage>/transform -d '{"input":"Hello, World!"}' -H 'Content-Type: application/json'
```

## Expected Output

Upon a successful POST request, the server will respond with a JSON payload echoing the transformed input. Given the example Rust application, the expected output is:

```json
{
  "message": "Transformed: Hello, World!"
}
```

![transform](./pic/transform.png)
